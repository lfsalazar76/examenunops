/*
  Script de bases de datos para la gestion de problemas.
  Creado: Enero 27 de 2021
  Por:
*/

/*Perfil de usuario*/
create table dbo.perfil
(
   perfil int not null identity(1,1),
   nombre char(30) not null,
   Descripcion char(60),
   primary key(perfil)
)

/* Datos del usuario*/
create table dbo.usuario
(
   usuario int not null identity(1,1),
   nombres char(30) not null,
   apellidos char(30) not null,
   perfil int not null,
   fingreso date,
   fultimamodificacion date,
   primary key (usuario),
   foreign key (perfil) references dbo.perfil(perfil)
)

/* Clasificacion del problema */
create table dbo.clasificacion
(
    clasificacion int not null identity(1,1),
    Clase char(50) not null,
	Descripcion char(80) not null
	primary key (clasificacion)
)

/* Estado del problema */
create table dbo.estado
(
    estado int not null identity(1,1),
    Nom_estado char(50) not null,
	Descripcion char(80)
	primary key (estado)
)

/* Tabla principal del problema */
create table dbo.problema
(
   problema int not null identity(1,1),
   Usuario int not null,
   fechaproblema date not null,
   clasificacion int not null,
   prioridad char(20) default 'Media',
   Asunto char(80) not null,
   horassoporte int not null default 0,
   estado int not null,
   primary key (problema),
   foreign key (usuario) references dbo.usuario (usuario),
   foreign key (clasificacion) references dbo.clasificacion(clasificacion),
   foreign key (estado) references dbo.estado(estado)
)


   
	
	
	
   