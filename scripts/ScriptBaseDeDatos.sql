USE [master]
GO
/****** Object:  Database [examenunops]    Script Date: 1/27/2021 6:11:30 PM ******/
CREATE DATABASE [examenunops]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'examenunops', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\examenunops.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'examenunops_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\examenunops_log.ldf' , SIZE = 2048KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [examenunops] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [examenunops].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [examenunops] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [examenunops] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [examenunops] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [examenunops] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [examenunops] SET ARITHABORT OFF 
GO
ALTER DATABASE [examenunops] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [examenunops] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [examenunops] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [examenunops] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [examenunops] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [examenunops] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [examenunops] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [examenunops] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [examenunops] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [examenunops] SET  DISABLE_BROKER 
GO
ALTER DATABASE [examenunops] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [examenunops] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [examenunops] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [examenunops] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [examenunops] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [examenunops] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [examenunops] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [examenunops] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [examenunops] SET  MULTI_USER 
GO
ALTER DATABASE [examenunops] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [examenunops] SET DB_CHAINING OFF 
GO
ALTER DATABASE [examenunops] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [examenunops] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [examenunops]
GO
/****** Object:  Table [dbo].[clasificacion]    Script Date: 1/27/2021 6:11:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[clasificacion](
	[clasificacion] [int] IDENTITY(1,1) NOT NULL,
	[Clase] [char](50) NOT NULL,
	[Descripcion] [char](80) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[clasificacion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[estado]    Script Date: 1/27/2021 6:11:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[estado](
	[estado] [int] IDENTITY(1,1) NOT NULL,
	[Nom_estado] [char](50) NOT NULL,
	[Descripcion] [char](80) NULL,
PRIMARY KEY CLUSTERED 
(
	[estado] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[perfil]    Script Date: 1/27/2021 6:11:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[perfil](
	[perfil] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [char](30) NOT NULL,
	[Descripcion] [char](60) NULL,
PRIMARY KEY CLUSTERED 
(
	[perfil] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[problema]    Script Date: 1/27/2021 6:11:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[problema](
	[problema] [int] IDENTITY(1,1) NOT NULL,
	[Usuario] [int] NOT NULL,
	[fechaproblema] [date] NOT NULL,
	[clasificacion] [int] NOT NULL,
	[prioridad] [char](20) NULL,
	[Asunto] [char](80) NOT NULL,
	[horassoporte] [int] NOT NULL,
	[estado] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[problema] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[usuario]    Script Date: 1/27/2021 6:11:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[usuario](
	[usuario] [int] IDENTITY(1,1) NOT NULL,
	[nombres] [char](30) NOT NULL,
	[apellidos] [char](30) NOT NULL,
	[perfil] [int] NOT NULL,
	[fingreso] [date] NULL,
	[fultimamodificacion] [date] NULL,
PRIMARY KEY CLUSTERED 
(
	[usuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[problema] ADD  DEFAULT ('Media') FOR [prioridad]
GO
ALTER TABLE [dbo].[problema] ADD  DEFAULT ((0)) FOR [horassoporte]
GO
ALTER TABLE [dbo].[problema]  WITH CHECK ADD FOREIGN KEY([clasificacion])
REFERENCES [dbo].[clasificacion] ([clasificacion])
GO
ALTER TABLE [dbo].[problema]  WITH CHECK ADD FOREIGN KEY([estado])
REFERENCES [dbo].[estado] ([estado])
GO
ALTER TABLE [dbo].[problema]  WITH CHECK ADD FOREIGN KEY([Usuario])
REFERENCES [dbo].[usuario] ([usuario])
GO
ALTER TABLE [dbo].[usuario]  WITH CHECK ADD FOREIGN KEY([perfil])
REFERENCES [dbo].[perfil] ([perfil])
GO
USE [master]
GO
ALTER DATABASE [examenunops] SET  READ_WRITE 
GO
