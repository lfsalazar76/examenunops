
--inciso A

select clasificacion.clase, count(Problema) as incidencias, sum(horassoporte) Totalhorassoporte 
from problema inner join clasificacion on (clasificacion.clasificacion = problema.clasificacion)
group by clasificacion.clase 

-- Inciso B


select perfil.nombre, 
       ltrim(rtrim(usuario.nombres)) + ' '  + ltrim(rtrim(usuario.apellidos)) as nombreusuario, 
	   problema.fechaproblema, 
	   clasificacion.clase, 
	   problema.prioridad, 
	   problema.asunto, 
	   problema.horassoporte,
	   estado.nom_estado
 from problema inner join usuario on (usuario.usuario = problema.usuario)
 inner join perfil on (perfil.perfil = usuario.perfil)
 inner join clasificacion on (clasificacion.clasificacion = problema.clasificacion)
 inner join  estado on (estado.estado = problema.estado)
 where problema.estado  =  2 

 -- Inciso C
 select perfil.nombre, problema.prioridad, count(Problema) conteoincidencias
from problema inner join usuario on (usuario.usuario = problema.usuario)
inner join perfil on (perfil.perfil = usuario.perfil)
group by perfil.nombre, problema.prioridad


 --Inciso D
select perfil.nombre, count(Problema) as incidencias
from problema inner join usuario on (usuario.usuario = problema.usuario)
inner join perfil on (perfil.perfil = usuario.perfil)
group by perfil.nombre
having count(problema) > 10

