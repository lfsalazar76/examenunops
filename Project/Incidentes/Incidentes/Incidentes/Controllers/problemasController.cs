﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Incidentes.Models;

namespace Incidentes.Controllers
{
    public class problemasController : Controller
    {
        private examenunopsEntities db = new examenunopsEntities();

        // GET: problemas
        public ActionResult Index()
        {
            var problemas = db.problemas.Include(p => p.clasificacion1).Include(p => p.estado1).Include(p => p.usuario1);
            return View(problemas.ToList());
        }

        // GET: problemas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            problema problema = db.problemas.Find(id);
            if (problema == null)
            {
                return HttpNotFound();
            }
            return View(problema);
        }

        // GET: problemas/Create
        public ActionResult Create()
        {
            ViewBag.clasificacion = new SelectList(db.clasificacions, "clasificacion1", "Clase");
            ViewBag.estado = new SelectList(db.estadoes, "estado1", "Nom_estado");
            ViewBag.Usuario = new SelectList(db.usuarios, "usuario1", "nombres");
            return View();
        }

        // POST: problemas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "problema1,Usuario,fechaproblema,clasificacion,prioridad,Asunto,horassoporte,estado")] problema problema)
        {
            if (ModelState.IsValid)
            {
                db.problemas.Add(problema);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.clasificacion = new SelectList(db.clasificacions, "clasificacion1", "Clase", problema.clasificacion);
            ViewBag.estado = new SelectList(db.estadoes, "estado1", "Nom_estado", problema.estado);
            ViewBag.Usuario = new SelectList(db.usuarios, "usuario1", "nombres", problema.Usuario);
            return View(problema);
        }

        // GET: problemas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            problema problema = db.problemas.Find(id);
            if (problema == null)
            {
                return HttpNotFound();
            }
            ViewBag.clasificacion = new SelectList(db.clasificacions, "clasificacion1", "Clase", problema.clasificacion);
            ViewBag.estado = new SelectList(db.estadoes, "estado1", "Nom_estado", problema.estado);
            ViewBag.Usuario = new SelectList(db.usuarios, "usuario1", "nombres", problema.Usuario);
            return View(problema);
        }

        // POST: problemas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "problema1,Usuario,fechaproblema,clasificacion,prioridad,Asunto,horassoporte,estado")] problema problema)
        {
            if (ModelState.IsValid)
            {
                db.Entry(problema).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.clasificacion = new SelectList(db.clasificacions, "clasificacion1", "Clase", problema.clasificacion);
            ViewBag.estado = new SelectList(db.estadoes, "estado1", "Nom_estado", problema.estado);
            ViewBag.Usuario = new SelectList(db.usuarios, "usuario1", "nombres", problema.Usuario);
            return View(problema);
        }

        // GET: problemas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            problema problema = db.problemas.Find(id);
            if (problema == null)
            {
                return HttpNotFound();
            }
            return View(problema);
        }

        // POST: problemas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            problema problema = db.problemas.Find(id);
            db.problemas.Remove(problema);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
